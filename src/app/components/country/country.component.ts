import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Recipes } from 'src/app/Module/Recipes';
import { BackendapiService } from 'src/app/services/backendapi.service';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {

  countries: Recipes[] = [];

  form: FormGroup;

  constructor(private countrySrv: BackendapiService) { }

  ngOnInit(): void {
    this.getAllCountries();
  }

  getAllCountries() {
    this.countrySrv.getAllFoods()
    .toPromise()
    .then((countries) => {
      console.log(countries);
      this.countries = countries;
    });
  }

}
