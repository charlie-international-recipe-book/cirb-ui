import { Component, OnInit } from '@angular/core';
import { BackendapiService } from 'src/app/services/backendapi.service';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {

  foods: any;
  countries: any;
  ingredients: any;

  constructor(private srv: BackendapiService) { }

  ngOnInit(): void {
    this.srv.getFoods();
    this.srv.getCountries();
    this.srv.getIngredients();
  }

  readFood() {
    this.foods = this.srv.getFoods().subscribe(x => this.foods = x);
  }

  readIngredient() {
    this.ingredients = this.srv.getIngredients().subscribe(x => this.ingredients = x);
  }

  readCountry() {
    this.countries = this.srv.getCountries().subscribe(x => this.countries = x);
  }

}
