import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DeleteComponent } from './components/delete/delete.component';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { CreateComponent } from './create/create.component';
import { EditComponent } from "./components/edit/edit.component";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReadComponent } from './components/read/read.component';
import { FoodComponent } from './components/food/food.component';
import { CountryComponent } from './components/country/country.component';
import { IngredientComponent } from './components/ingredient/ingredient.component';

@NgModule({
  declarations: [
    AppComponent,
    DeleteComponent,
    // CreateComponent,
    EditComponent,
	ReadComponent,
    FoodComponent,
    CountryComponent,
    IngredientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

