import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeleteComponent } from './components/delete/delete.component';
import { EditComponent } from './components/edit/edit.component';
import { ReadComponent } from './components/read/read.component';

const routes: Routes = [
  {
    path: "read",
    component: ReadComponent
  },
  // {
  //   path: "create",
  //   component: CreateComponent
  // },
  {
    path: "edit",
    component: EditComponent
  },
  {
    path: "delete",
    component: DeleteComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
